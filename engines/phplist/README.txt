********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: phplist module
Author: Aaron Welch <welch at advomatic dot com>
Last update: January 08, 2005 (See CHANGELOG for details)
Drupal: CVS
Dependencies:
  massmailer.module
  phplist - see INSTALL.txt file for details

********************************************************************
DESCRIPTION:

Enables phplist to be utilized as a backend for the massmailer module
This module will only run on a system that can execute a bash script
from a shell_exec call from php

********************************************************************
INSTALLATION:

see the INSTALL file in this directory.

********************************************************************
WISH LIST:
