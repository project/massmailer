In order to keep Drupal's cvs server clean and reduce problems with
synchronizing development efforts with the phplist project you must
download http://www.phplist.com/files/phplist-2.9.4.tgz and copy the
'public_html/lists/admin' directory from it into this directory. 
