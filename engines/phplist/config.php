<?php
/*
 * this function was copied from bootstrap.inc,v 1.32.2.3 2004/12/01 12:09:56 dries Exp $
 * it has been modified to collect the server information from the exec call instead of the $_SERVER global
 * it was renamed from conf_init() to drupal_conf_init() to avoid conflicts with phplist
*/
function drupal_conf_init() {
  $args = parseCline();
  $uri = $args['d'];

  $site = strtolower(strtr($args['h'] . substr($uri, 0, strrpos($uri, '/')), '/:', '..'));

  while (strlen($site) > 4) {
    if (file_exists('../../../../../../sites/'. $site .'/settings.php')) {
      return $site;
    }
    else {
      $site = substr($site, strpos($site, '.') + 1);
    }
  }

  return 'default';
}

/*
 * parses database information into phplist format from drupal format
 */
function drupal_database_init() {
  $drupal_config_file = drupal_conf_init();
  include_once '../../../../../../sites/'. $drupal_config_file .'/settings.php';

  $db_info = array();
  if(!$db_url) {
    ob_clean();
    die("The database configuration information could\nnot be loaded from the drupal configuration file:\n". $drupal_config_file .'/settings.php');
  }
  elseif((!is_array($db_url) && substr($db_url, 0, 8) != 'mysql://') || (is_array($db_url) && (substr($db_url['default'], 0, 8) != 'mysql://' && substr($db_url['phplist'], 0, 8) != 'mysql://'))) {
    ob_clean();
    die("A database url for a database other than mysql has been detected in your drupal installation.\nPHPList requires the MySQL database server.");
  }
  else {
    if (is_array($db_prefix)) {
      $db_info_prefix = isset($db_prefix['phplist']) ? $db_prefix['phplist'] : $db_prefix['default'];
    }
    else {
      $db_info_prefix = $db_prefix;
    }
    $dbstr = is_array($db_url) ? (isset($db_url['phplist']) ? $db_url['phplist'] : $db_url['default']) : $db_url;
    $db_info = parse_url($dbstr);
    $db_info['name'] = substr($db_info['path'], 1);
    $db_info['prefix'] = $db_info_prefix;
  }

return $db_info;
}

$db_info = drupal_database_init();

if (isset($db_info['port'])) {
  $database_host = $db_info['host'] . ':' . $db_info['port'];
}
else {
  $database_host = $db_info['host'];
}
$database_name = $db_info['name'];
$database_user = $db_info['user'];
$database_password = $db_info['pass'];
$table_prefix = $db_info['prefix']."phplist_";
$usertable_prefix = $db_info['prefix']."phplist_user_";

// stable vars, these will not be overwritten

// set this to 1 to only simulate message sending, will not actually send emails.
define ("TEST",0);

# set this to 1 if you want PHPlist to deal with login for the administrative
# section of the system
# you will be able to add administrators who control their own lists
# default login is "admin" with password "phplist"
#
$require_login = 1;

// Drupal sends mails in 8bit mode
define('TEXTEMAIL_ENCODING','8bit');

// We use PHPmailer for sending.
define("PHPMAILER",1);

// increase reporting verbosity, apparently this can crash some browsers with large email batches
define ("VERBOSE",0);

// these are relative to the document root as you can see, if changed nothing will work. Enjoy!
$pageroot = '/modules/massmailer/engines/phplist/phplist/';
$adminpages = '/modules/massmailer/engines/phplist/phplist/admin';

# select the language module to use
# Look for <country>.inc files in the texts directory
# to find your language
#
// this is not really going to do anything except keep phplist from complaining
$language_module = "english.inc";

# if you use multiple installations of PHPlist you can set this to
# something to identify this one. it will be prepended to email report
# subjects
$installation_name = 'PHPlist';

# some warnings may show up about your PHP settings. If you want to get rid of them
# set this value to 0
define ("WARN_ABOUT_PHP_SETTINGS",1);

# If you set up your system to send the message automatically, you can set this value
# to 0, so "Process Queue" will disappear from the site
# this will also stop users from loading the page on the web frontend, so you will
# have to make sure that you run the queue from the commandline
# check README.commandline how to do this
// not sure if this is necessarry, but I suppose it can't hurt
define ("MANUALLY_PROCESS_QUEUE",1);

# if you want to use \r\n for formatting messages set the 0 to 1
# see also http://www.securityfocus.com/archive/1/255910
# this is likely to break things for other mailreaders, so you should
# only use it if all your users have Outlook (not Express)
define("WORKAROUND_OUTLOOK_BUG",0);

# CREDITS
# We request you retain some form of credits on the public elements of
# PHPlist. These are the subscribe pages and the emails.
# This not only gives respect to the large amount of time given freely
# by the developers	but also helps build interest, traffic and use of
# PHPlist, which is beneficial to future developments.
# By default the webpages and the HTML emails will include an image and
# the text emails will include a powered by line

# If you want to remove the image from the HTML emails, set this constant
# to be 1, the HTML emails will then only add a line of text as signature
define("EMAILTEXTCREDITS",0);

# batch processing
# if you are on a shared host, it will probably be appreciated if you don't send
# out loads of emails in one go. To do this, you can configure batch processing.
# Please note, the following two values can be overridden by your ISP by using
# a server wide configuration. So if you notice these values to be different
# in reality, that may be the case

# define the amount of emails you want to send per period. If 0, batch processing
# is disabled
define("MAILQUEUE_BATCH_SIZE",0);

# define the length of one batch processing period, in seconds (3600 is an hour)
define("MAILQUEUE_BATCH_PERIOD",3600);

/*

=========================================================================

Advanced Features, HTML editor, RSS, Attachments, Plugins. PDF creation

=========================================================================

*/

# you can specify the encoding for HTML messages here. This only works if you do
# not use the phpmailer (see below)
# the default should be fine. Valid options are 7bit, quoted-printable and base64
define("HTMLEMAIL_ENCODING","quoted-printable");

# PHPlist can send RSS feeds to users. Feeds can be sent daily, weekly or
# monthly. This feature is currently marked experimental.
# To use the feature you need XML support in your PHP installation, and you need
# to set this constant to 1
define("ENABLE_RSS",0);

# if you have set up a cron to download the RSS entries, you can set this to be 0
define("MANUALLY_PROCESS_RSS",1);

# Repetition. This adds the option to repeat the same message in the future.
# After the message has been sent, this option will cause the system to automatically
# create a new message with the same content. Be careful with it, because you may
# send the same message to your users
# the embargo of the message will be increased with the repetition interval you choose
# also read the README.repetition for more info
define("USE_REPETITION",0);

# If you do the above, and you want to send with SMTP, give your SMTP server here:
# otherwise the normal mail() function will be used
define("SMTPHOST","mail");

# if you upgrade we need to be able to write temporary files somewhere
# indicate where this can be done. Make sure it is writable by your webserver
# user. Linux users can leave it as it is.
# If you send messages as PDF, you will need to use this as well
# also the RSS class will create cache files in this directory
$tmpdir = '/tmp';

# if you are on Windoze, and/or you are not using apache, in effect when you are getting
# "Method not allowed" errors you will want to uncomment this
# ie take off the #-character in the next line
# using this is not guaranteed to work, sorry. Easier to use Apache instead :-)
# $form_action = 'index.php';

# select the database module to use
# anyone wanting to submit other database modules is
# very welcome!
$database_module = "mysql.inc";

# there is now support for the use of ADOdb http://php.weblogs.com/ADODB
# this is still experimental, and any findings should be reported in the
# bugtracker
# in order to use it, define the following settings:
#$database_module = 'adodb.inc';
#$adodb_inc_file = '/path/to/adodb_inc.php';
#$adodb_driver = 'mysql';

# if you want more trouble, make this 63 (very unlikely you will like the result)
$error_level = error_reporting(0);

################################################################################################
# you should not need to edit below, but if things go wrong you can try to play around with it #
################################################################################################

if (file_exists($database_module)) {
  include $database_module;
}
else if (file_exists("admin/$database_module")) {
  include "admin/$database_module";
}
else if (file_exists("../$database_module")) { # help is one level up
  include "../$database_module";
}
else {
	print "Cannot load $database_module, exit";
	exit;
}
if (file_exists("../texts/english.inc")) { # first load english and then the translation
  include "../texts/english.inc";
}
else if(file_exists("texts/english.inc")) {
  include "texts/english.inc";
}
else if (file_exists("../../texts/english.inc")) {
  include "../../texts/english.inc";
}
else {
	print "Cannot load english.inc, exit;";
	exit;
}
if (file_exists("../texts/$language_module")) {
  include "../texts/$language_module";
}
else if (file_exists("texts/$language_module")) {
  include "texts/$language_module";
}
else if (file_exists("../../texts/$language_module")) {
  include "../../texts/$language_module";
}
else {
	print "Cannot load $language_module, exit";
	exit;
}
if (file_exists("defaultconfig.inc")) {
  include "defaultconfig.inc";
}
else if (file_exists("admin/defaultconfig.inc")) {
  include "admin/defaultconfig.inc";
}
else if (file_exists("../defaultconfig.inc")) {
  include "../defaultconfig.inc";
}
else {
  print "cannot load defaultconfig, exit";
	exit;
}
if (file_exists("connect.php")) {
  include "connect.php";
}
else if (file_exists("admin/connect.php")) {
  include "admin/connect.php";
}
else if (file_exists("../connect.php")) {
  include "../connect.php";
}
else {
	print "Cannot load libraries connect, exit";
	exit;
}
if (file_exists("languages.php")) {
  include "languages.php";
}
else if (file_exists("admin/languages.php")) {
  include "admin/languages.php";
}
else if (file_exists("../languages.php")) {
  include "../languages.php";
}
else {
	print "Cannot load language libraries, exit";
	exit;
}#ob_start(); # useful to be able to redirect after outputting

$result = Sql_fetch_array(Sql_query("SELECT value FROM ". $db_info['prefix']."variable WHERE name = 'phplist_command_line_user'"));
$commandline_users[] = unserialize($result['value']);

$result = Sql_fetch_array(Sql_query("SELECT value FROM ". $db_info['prefix']."variable WHERE name = 'phplist_batch_size'"));
define("MAILQUEUE_BATCH_SIZE",unserialize($result['value']));

$result = Sql_fetch_array(Sql_query("SELECT value FROM ". $db_info['prefix']."variable WHERE name = 'phplist_batch_period'"));
define("MAILQUEUE_BATCH_PERIOD",unserialize($result['value']));
?>
