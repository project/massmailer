********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: massmailer module
Author: Aaron Welch <welch at advomatic dot com>
Last update: August 18, 2005 (See CHANGELOG for details)
By: Gerhard Killesreiter
Drupal: 4.6
Dependencies:
  requires one of the engine modules located in the engines directory
Supports:
  CiviCRM

********************************************************************
DESCRIPTION:

Provides a mass mailing interface for a variety of mailer backends

********************************************************************
INSTALLATION:

see the INSTALL.txt file in this directory.

********************************************************************
WISH LIST:

- Additional mailer engines
