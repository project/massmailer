This module adds templating functionality to MassMailer. It requires massmailer.module (and engine) as well as massmailertemplate table in drupal database (see INSTALL.txt).

It has been debated exactly what "template" means, but in this case it just means: saved email messages that you can load in and modify before sending. So other than the macros included by PHPList, of which there are several, this module doesn't add much in terms of mail-merge type functionality. I recommend making templates with a body that looks something like this:

***************************
Greetings from UltraMegaCorp!

We've got some great new whizbangs this month and here's this month's list:

[[ PUT CONTENT HERE ]]

Thanks for shopping UltraMegaCorp.

Love,
Timmy
****************************

This really becomes powerful when using a WYSIWYG (like tinyMCE.module) to edit HTML messages. Users can add content in a very Word-meets-Dreamweaver environment and see exactly what the messages will look like before they get sent.

Jeff Robbins
jeff (at) jjeff (d0t) com

