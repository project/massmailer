Once enabled, MassMailer will automatically maintain a mailing list for each role on your site. As users are assigned and reassigned different roles, they will automatically be added or removed from the appropriate MassMailer lists. 

Jeff Robbins
robbins (at) jjeff (d0t) com

