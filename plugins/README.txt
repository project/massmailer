********************************************************************
       M A S S M A I L E R    M O D U L E    P L U G I N S          
********************************************************************
Dependencies:
  These plugins generally require a properly configured and working 
  massmailer module.

********************************************************************
DESCRIPTION:

The plugins in this directory all extend massmailer.module's 
functionality in some way. They are maintained by thier respective 
authors, and may not be tested by the massmailer maintainers. Please 
install and use with care.

********************************************************************
INSTALLATION:

see the INSTALL.txt files in the individual plugin directories.

********************************************************************
